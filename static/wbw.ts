const wbw = {
  "data": [
    {
      "name": "Escherichia coli",
      "strain": "K-12",
      "versions": [ 
        "GCF_000005845.2"
      ],
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 0
        },
        {
          "name": "styk",
          "count": 2
        },
        {
          "name": "cdigmp",
          "count": 29
        },
        {
          "name": "mcp",
          "count": 5
        },
        {
          "name": "hk",
          "count": 30
        },
        {
          "name": "ecf",
          "count": 2
        }
      ]
    },
    {
      "name": "Bacillus subtilis",
      "strain": "168",
      "versions": [
        "GCF_000789275.1",
        "GCF_000009045.1"
      ],
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 5
        },
        {
          "name": "styk",
          "count": 4
        },
        {
          "name": "cdigmp",
          "count": 6
        },
        {
          "name": "mcp",
          "count": 10
        },
        {
          "name": "hk",
          "count": 36
        },
        {
          "name": "ecf",
          "count": 7
        }
      ]
    },
    {
      "name": "Mycobacterium tuberculosis",
      "strain": "H37Rv",
      "versions": [
        "GCF_000195955.2",
        "GCF_000277735.2",
        "GCF_000831245.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 2
        },
        {
          "name": "styk",
          "count": 13
        },
        {
          "name": "cdigmp",
          "count": 2
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 14
        },
        {
          "name": "ecf",
          "count": 10
        }
      ]
    },
    {
      "name": "Porphyromonas gingivalis",
      "strain": "W83",
      "versions": [
        "GCF_000007585.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 0
        },
        {
          "name": "styk",
          "count": 0
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 6
        },
        {
          "name": "ecf",
          "count": 6
        }
      ]
    },
    {
      "name": "Neisseria gonorrhoeae",
      "strain": "FA 1090",
      "versions": [
        "GCF_000006845.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 0
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 4
        },
        {
          "name": "ecf",
          "count": 1
        }
      ]
    },
    {
      "name": "Streptococcus pyogenes",
      "strain": "M1 GAS",
      "versions": [
        "GCF_000006785.2"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 1
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 12
        },
        {
          "name": "ecf",
          "count": 2
        }
      ]
    },
    {
      "name": "Haemophilus influenzae",
      "strain": "Rd KW20",
      "versions": [
        "GCF_000027305.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 0
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 4
        },
        {
          "name": "ecf",
          "count": 2
        }
      ]
    },
    {
      "name": "Helicobacter pylori",
      "strain": "26695",
      "versions": [
        "GCF_000008525.1",
        "GCF_000307795.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 1
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 4
        },
        {
          "name": "hk",
          "count": 4
        },
        {
          "name": "ecf",
          "count": 0
        }
      ]
    },
    {
      "name": "Treponema pallidum",
      "strain": "str. Nichols",
      "versions": [
        "GCF_000410535.2"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 3
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 1
        },
        {
          "name": "mcp",
          "count": 4
        },
        {
          "name": "hk",
          "count": 1
        },
        {
          "name": "ecf",
          "count": 1
        }
      ]
    },
    {
      "name": "Rickettsia typhi",
      "strain": "str. Wilmington",
      "versions": [
        "GCF_000008045.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 0
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 2
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 4
        },
        {
          "name": "ecf",
          "count": 0
        }
      ]
    },
    {
      "name": "Chlamydia trachomatis",
      "strain": "D/UW-3/Cx",
      "versions": [
        "GCF_000008725.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 3
        },
        {
          "name": "styk",
          "count": 0
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 1
        },
        {
          "name": "ecf",
          "count": 0
        }
      ]
    },
    {
      "name": "Mycoplasma pneumoniae",
      "strain": "M129",
      "versions": [
        "GCF_000027345.1"
      ], 
      "wbwSigStat": [
        {
          "name": "pp2c",
          "count": 1
        },
        {
          "name": "styk",
          "count": 1
        },
        {
          "name": "cdigmp",
          "count": 0
        },
        {
          "name": "mcp",
          "count": 0
        },
        {
          "name": "hk",
          "count": 0
        },
        {
          "name": "ecf",
          "count": 1
        }
      ]
    }
  ]
}

export { wbw }