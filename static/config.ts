const config = {
  info: {
    "hk": {
      pretty: "Histidine Kinase",
      notes: "include hybrids and CheAs",
    },
    "ecf": {
      pretty: "Extracytoplasmic Functions",
      notes: "ECF",
    },
    "mcp": {
      pretty: "Methyl-accepting Chemotaxis Proteins",
      notes: "MCP",
    },
    "cdigmp": {
      pretty: "ci-di-GMP signaling",
      notes: "GGDEF + (GGDEF+EAL) + EAL, no HD-GYP",
    },
    "styk": {
      pretty: "STYK",
      notes: "Pkinase and Pkinase_Tyr"
    },
    "pp2c": {
      pretty: "PP2C",
      notes: "include SpoIIE"
    }
  },
  filters: {
    hk: [
      {
        field: 'ranks',
        value: 'hk'
      },
      {
        field: 'ranks',
        value: 'hhk'
      },
      {
        field: 'ranks',
        value: 'chea'
      },
    ],
    ecf: [
      {
        field: 'ranks',
        value: 'ecf',
      },
    ],
    mcp: [
      {
        field: 'ranks',
        value: 'mcp',
      },
    ],
    cdigmp: [
      {
        field: 'outputs',
        value: 'GGDEF,EAL'
      }
    ],
    styk: [
      {
        field: 'outputs',
        value: 'Pkinase,Pkinase_Tyr'
      }
    ],
    pp2c: [
      {
        field: 'outputs',
        value: 'SpoIIE,PP2C'
      }
    ]
  },
  tableOrder: [
    'hk',
    'mcp',
    'cdigmp',
    'ecf',
    'styk',
    'pp2c'
  ],
  colorCode: {
    fp: 'red',
    fn: 'purple',
    hit: 'green'
  }
}

export {
  config
}