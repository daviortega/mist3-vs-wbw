# mist3-vs-wbw

> Automagic control for MiST3 assignments of signal transduction genes against WBW

[Live here](https://daviortega.gitlab.io/mist3-vs-wbw)


## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
