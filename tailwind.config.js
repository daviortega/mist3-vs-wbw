/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      height: {
        log: '500px'
      },
      colors: {
        'mist-navbar': '#197477',
        'transp-grey': 'rgba(20,20,20,0.6)'
      },
      fontFamily: {
        legend: [ 'Roboto', '"Helvetica Neue"' ]
      }
    }
  },
  variants: {},
  plugins: []
}
